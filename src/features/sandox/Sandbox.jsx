import React, {useState} from "react";
import { useDispatch, useSelector } from "react-redux";
import { openModal } from "../../app/common/modals/modalReducer";
import {increment, decrement} from './testReducer';

export default function Sandbox(){
    const dispatch = useDispatch();
    const [taget, setTarget] = useState(null) //for loading spinner 
    const data = useSelector(state => state.test.data) 
    // const {loading} = useSelector((state) => state.aync)  //for loading spinner but its not showing  

    return(
        <>
            <h1>Tesing 123</h1>
            <h3>The data is: {data}</h3>
            <button 
                name="increment"
                // loading={loading && target === 'increment'}
                onClick={(e) => {
                    dispatch(increment(20));
                    setTarget(e.target.name)
                }} 
                style={{ background: "green"}}>Increment</button>
            <button 
                name="decrement"
                // loading = {loading && target === 'decrement'}
                onClick={(e) => { 
                    dispatch(decrement(10));
                    setTarget(e.target.name)
                }} 
                style={{ background: "red"}}>Decrement</button>
            <button 
                onClick={() => dispatch(openModal({modalType:'TestModal', modalProps: {data}}))} 
                style={{ background: "orange"}}
            >Open Modal</button>
           
        </>
    )
}
