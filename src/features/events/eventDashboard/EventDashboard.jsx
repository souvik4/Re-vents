import React from "react";
import { useSelector } from "react-redux";
import { Grid } from "semantic-ui-react";
import EventFilters from "./EventFilters";
// import EventForm from "../eventForm/EventForm";
import EvenList from "./EventList";
import EventListItemPlaceholder from "./EventListItemPlaceholder";

export default function EventDashboard(props){

    const {events} = useSelector(state => state.event)    
    const {loading} = useSelector(state => state.async);

    
    return(
        
    <div>
        <Grid>
            <Grid.Column width={10}>
                {loading &&
                    <>
                        <EventListItemPlaceholder />
                        <EventListItemPlaceholder />
                    </> 
                }
                <EvenList events={events} />
            </Grid.Column>
            <Grid.Column width={6}>
                <EventFilters />
            </Grid.Column>
        </Grid>
           
        

    </div>
    )
}