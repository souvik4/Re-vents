import React from "react";
import EvenListItem from "./EvenListItem";

export default function EvenList(props){
  const events = props.events;

    return (
      <> 
        {events.map(event => (
          <EvenListItem event={event} key={event.id} />
        ))}
      </> 
    )
}