import React from 'react'
import Calendar from 'react-calendar';
import './EventFilters.css'

export default function EventFilters(){
    return(
        <div className='div' style={{ paddingBottom:"10px", marginBottom:"20px"}}>
            <div vertical size='large'>
                <h2 icon='filter' style={{background: "teal"}}>Filter</h2>
                <p>All Event</p>
                <p>I'm going</p>
                <p>I'm hoisting</p>
            </div>
            <h2 icon='calender' style={{background: "teal"}}>selected date</h2>
            <Calendar/>
        </div>
    )
}