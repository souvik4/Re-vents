import React from "react";
import EvenListAttendee from "./EventListAttendee";
import { AiFillClockCircle} from "react-icons/ai";
import { IoIosCompass} from "react-icons/io";
import { Link } from "react-router-dom";
import { Button } from "semantic-ui-react";
import { useDispatch } from "react-redux";
import {deleteEvent} from '../eventActions'

export default function EvenListItem({event}){  
    const dispatch = useDispatch();

    return (
        
        <div style={{border: "2px solid black", marginBottom: "20px", background:"#A12DEC"}}>
            <img src = {event.hostPhotoURL} alt="logo" style={{width: "50px", height: "50px", margin: "10px"}}/>
            <h1 style={{margin: "10px"}}>{event.title}</h1>
            <p style={{margin: "10px"}}> Hosted by {event.hostedBy}</p>
                <AiFillClockCircle/> {event.date}
                <IoIosCompass/> {event.venue}
                <br/>
            
                <div style={{margin: "10px"}}>
                {event.attendees.map(attendee => (
                    <EvenListAttendee key={attendee.id} attendee={attendee}/>
                ))}
                </div>
            
            <br/>
            <span style={{margin: "10px"}}>{event.description}</span>
            <br/>

            <Button 
                as={Link} to={`/events/${event.id}`}
                style={{background:"#22BED6", margin: "10px", padding: "10px"}}
            >view</Button>
            

            <button 
                onClick={()=>dispatch(deleteEvent(event.id))} 
                style={{background:" #FF0000 ", margin: "10px", padding: "10px"}}
            >Delete</button>
        </div>
    )
}