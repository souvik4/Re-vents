import React from "react";
import './EventForm.css'
import cuid from "cuid";
import { Link } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { createEvent, updateEvent } from "../eventActions";
import { Formik, Form } from 'formik';
import * as Yup from 'yup'
import MyTextInput from "../../../app/common/form/MyTextInput";
import MyTextArea from "../../../app/common/form/MyTextArea";
import MySelectInput from "../../../app/common/form/MySelectInput";
import { categoryData } from "../../../app/api/categoryOptions";
import MyDateInput from "../../../app/common/form/MyDateInput";

export default function EventForm({match, history}) {
    const dispatch = useDispatch();

    const selectedEvent = useSelector(state => state.event.events.find(e => e.id === match.params.id))

    const initialValues = selectedEvent ?? {
        title: '',
        category: '',
        description: '',
        city: '',
        venue: '',
        date: ''
    }

    const validationSchema = Yup.object({
        title: Yup.string().required('You must Provide a title'),
        category: Yup.string().required('You must Provide a category'),
        description: Yup.string().required(),
        city: Yup.string().required(),
        venue: Yup.string().required(),
        date: Yup.string().required()
    })    
   

    return(
        <>
        <Formik 
            initialValues={initialValues}
            validationSchema={validationSchema}
            onSubmit={(values) => {
                selectedEvent 
                ? dispatch(updateEvent({...selectedEvent, ...values})) 
                : dispatch(createEvent({
                    ...values, 
                    id: cuid(), 
                    hostedBY:'Bob', 
                    attendees:[], 
                    hostPhotoURL: '/public/assets/user.png'
                    })
                );
                history.push('/events')
            }}
            >
            {({isSubmitting, dirty, isValid}) => (
                <Form >
                    <h1>Event Details</h1>

                    <MyTextInput name='title' placeholder="Event title" />
                    <MySelectInput  name='category' placeholder="Category" options={categoryData}/>
                    <MyTextArea name='description' placeholder="Description" rows={3} />
                    <h1>Event Location Details</h1>
                    <MyTextInput  name='city' placeholder="City" />
                    <MyTextInput  name='venue' placeholder="Venue" />
                    <MyDateInput  
                        name='date' 
                        placeholderText="Event Date" 
                        timeFormat='HH:mm'
                        showTimeSelect
                        timeCaption='time'
                        dateFormat='MMMM d, yyyy h:mm a'
                    />
        
        
                    <br/>
                    <button 
                        style={{marginLeft: "30px", background: "green", margin:"20px", padding: "10px"}}
                        loading={isSubmitting}
                        disabled={!isValid || !dirty || isSubmitting}
                    >Submit</button>
        
                    <button 
                        as={Link} 
                        to='/events' 
                        style={{marginLeft: "20px", background: "red", padding: "10px"}}
                        disabled={isSubmitting} //My bottom is not showing disablity but it workd as diable
                    >Cancel</button>
                </Form>
            )}
            
        
        </Formik>
        </>
    )
}
