import React from "react";
import { Link } from "react-router-dom";
import { Segment, Button, Image, Item, Header } from "semantic-ui-react";
import './EventDetailedHeader.css'
const eventImageStyle = {
    filter: 'brightness(30%)',
    height:'20%',
    width: '20%',
};

const eventImageTextStyle = {
    position: 'relative',
    bottom: '50%',
    left: '30%',
    width: '100%',
    height: 'auto',
    color: 'Black'
};

export default function EventDetailedHeader({event}){
    return(
        <Segment.Group className="border">
        <Segment basic attached="top" style={{padding: '0'}}>
            <Image src={`/assets/categoryImages/${event.category}.jpg`} fluid style={eventImageStyle}/>

            <Segment basic style={eventImageTextStyle}>
                <Item.Group>
                    <Item>
                        <Item.Content>
                            <Header
                                content={event.title}
                                style={{color: 'Black', fontSize:"50px"}}
                            />
                            <p>{event.date}</p>
                            <p>
                                Hosted by <strong>{event.hostedBy}</strong>
                            </p>
                        </Item.Content>
                    </Item>
                </Item.Group>
            </Segment>
        </Segment>

        <Segment attached="bottom">
            <Button style={{background:"red"}}>Cancel My Place</Button>
            <Button style={{background:"teal"}}>JOIN THIS EVENT</Button>

            <Button as={Link} to={`/manage/${event.id}`}style={{background: "orange", padding: "10px"}}   floated="right">
                Manage Event
            </Button>
        </Segment>
    </Segment.Group>
    )
}