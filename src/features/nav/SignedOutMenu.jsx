import React from "react";
import { Button, Menu } from "semantic-ui-react";
import { openModal } from "../../app/common/modals/modalReducer";
import { useDispatch } from "react-redux";

export default function SignedOutMenu({setAuthenticated}){
    const dispatch = useDispatch()
    return(
        <Menu.Item className="butu">
            <Button onClick={() => dispatch(openModal({modalType: 'LoginForm'}))} possitive inverted content='Login'style={{background: "green", padding: "10px"}}/>
            <Button possitive inverted content='Register' style={{ marginLeft: "10px", marginBottom: '20px', background: "orange", padding: "10px"}}/>
        </Menu.Item>
    )
}
// onClick={() => setAuthenticated(true)}
//onClick={() => dispatch(openModal({modalType: 'LoginForm'}))}
 