import React from "react";
import { Dropdown } from "semantic-ui-react";
import { Menu, Image } from "semantic-ui-react";
import {Link, useHistory} from "react-router-dom"
import { useDispatch, useSelector } from "react-redux";
import { signOutUser } from "../auth/authActions";

export default function SignedInMenu(){
    const dispatch = useDispatch();
    const {currentUser} = useSelector(state => state.auth)
    const history = useHistory()
    return(
        <Menu.Item className="butu">
            <Image avter spaced='right' src={currentUser.photoURL || '/assets/user.png'} style={{height: "30px", width: "30px", marginTop: "20px", borderRadius: "50%"}}/>
            <Dropdown pointing='top left' text={currentUser.email}>
                <Dropdown.Menu>
                    <Dropdown.Item as={Link} to='/createEvent' text='Create Event' icon='plus' style={{color: "orange"}}/>
                    <Dropdown.Item text='My Profile' icon='user'/>
                    <Dropdown.Item onClick={() => {
                            dispatch(signOutUser());
                            history.push('/')
                            }
                        } text='Sign Out' icon='power' style={{cursor: "pointer"}}/>
                </Dropdown.Menu>
            </Dropdown>
        </Menu.Item>
    )
}
// onClick={signOut}
