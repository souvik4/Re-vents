import React from "react"; 
import { Button, Container, Menu } from "semantic-ui-react";
import './NavBar.css';
import { NavLink} from "react-router-dom";
import SignedOutMenu from "./SignedOutMenu";
import SignedInMenu from "./SignedInMenu";
import { useSelector } from "react-redux";

export default function NavBar({setFormOpen}){
    // const history = useHistory();
    // const [authenticated, setAuthenticated] = useState(false);
    const {authenticated} = useSelector(state => state.auth)

    // function HandleSignout(){
        // setAuthenticated(false);
    //     history.push('/')  
    // }

    return(
        <Menu inverted fixed='top'>
            <Container>
                <Menu.Item as={NavLink} to='/' header>
                    <img src="/assets/logo.png" alt="logo" style={{ marginLeft: "15px"}}/>
                    <p style={{color: "orange"}}>Re-vents</p>
                </Menu.Item>
                <Menu.Item as={NavLink} to='/events' name='Events' style={{color: "orange"}}/>
                <br/>
                <Menu.Item as={NavLink} to='/sandbox' name='Sandbox' style={{color: "orange"}}/>


                {authenticated && 
                <Menu.Item as={NavLink} to='/createEvent'>
                    <Button content='Create Event' style={{marginLeft: '0.5rem',marginTop: '2rem', background: "violet", padding: "10px"}}/>
                </Menu.Item>}

                {authenticated ? ( 
                    <SignedInMenu />
                    ) : ( 
                    <SignedOutMenu />
                    
                    )}
              
               
            </Container>
        </Menu>
    );
}
