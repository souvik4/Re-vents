import React from "react";
import './HomePage.css'
import { IoMdArrowRoundForward } from "react-icons/io";


export default function HomePage({history}){
    return(
        <div class="home">
            <img src='/assets/logo.png' alt="logo" />
            <h1>Re-vents</h1>
            <button onClick={() => history.push('/events')}>
                Get started  
                <IoMdArrowRoundForward />
            </button>
        </div>
    )
}