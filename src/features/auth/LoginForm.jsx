// import React from 'react'
// import ModalWrapper from '../../app/common/modals/ModalWrapper'
// import * as Yup from 'yup'
// import MyTextInput from '../../app/common/form/MyTextInput'
// // import { Formik,Form } from 'formik'
// // import { Form } from 'react-final-form'
// // import Formsy from 'formsy-react';

// export default function LoginForm() {
//     const isSubmitting = true;
//     const isValid = true;
//     const dirty = true;



//     return(     
//         <ModalWrapper header='Sign in to Re-vents'>
//             <form
//             // initialValues={{email: '', password: ''}}
//             // validationSchema={Yup.object({
//             //     email: Yup.string().required().email(),
//             //     password: Yup.string().required()
//             // })}
//             // onSubmit={values => {
//             //     console.log(values);
//             // }}
//            >
//                 {/* {({isSubmitting, isValid, dirty}) => ( */}

//                     <>
//                         <input name='email' placeholder='Email Address' />
//                         <input name='password' placeholder='password' type='password' />
//                         <button
//                             loading={isSubmitting}
//                             disabled={!isValid || !dirty || isSubmitting} 
//                             type='submit'
//                             // fluid
//                             style={{background: 'teal'}}
//                         >Login</button>
//                     </>
//                  {/* )}  */}
                
//             </form>
            
//         </ModalWrapper>
//     )
// }


import React from 'react'
import { useDispatch } from 'react-redux';
import { closeModal } from '../../app/common/modals/modalReducer';
import ModalWrapper from '../../app/common/modals/ModalWrapper'
import { signInUser } from './authActions';


const FooBarForm = () => {

    const initialFormData = Object.freeze({
        email: "",
        password: ""
    });
  
    const [formData, updateFormData] = React.useState(initialFormData);
    const dispatch = useDispatch();

    const handleChange = (e) => {
      updateFormData({
                ...formData,
                [e.target.name]: e.target.value.trim()
            });
            };
        
            const handleSubmit = (e) => {
            e.preventDefault()
            console.log(formData);
            dispatch(signInUser(e));
            dispatch(closeModal())
             
            };
  
    return (
        <ModalWrapper header='Sign in to Re-vents'>
        <form>
            <label>
                Email
                <input name="email" onChange={handleChange} />
            </label>
            <br />
            <label>
                Password
                <input name="password" onChange={handleChange} />
            </label>
            <br />
            <button onClick={handleSubmit}>Login</button>
            </form>
      </ModalWrapper >
    );
  };
  
  export default FooBarForm;