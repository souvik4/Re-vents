import React from "react";
import {useField} from 'formik';


export default function MyTextArea({label, ...props}){
    const [field, meta] = useField(props)
    return(
        <div error={meta.touched && !!meta.error}>
            <label>{label}</label>
            <textarea {...field} {...props}/>
            {meta.touched && meta.error ? (
                <div style={{color: 'red', marginLeft:"20px"}}>{meta.error}</div>
            ) :null }
        </div>
    )
}
