import React from "react";
import {useField} from 'formik';
import { Select } from "semantic-ui-react";


export default function MySelectInput({label, ...props}){
    const [field, meta, helpers] = useField(props)
    return(
        <div error={meta.touched && !!meta.error}>
            <label>{label}</label>            
            <Select
                 value={field.value || null}
                 onChange={(e, d) => helpers.setValue(d.value)}
                 onBlur={() => helpers.setTouched(true)}
                 {...props}
            />

            
            {meta.touched && meta.error ? (
                <div style={{color: 'red', marginLeft:"20px"}}>{meta.error}</div>
            ) :null }
        </div>
    )
}
