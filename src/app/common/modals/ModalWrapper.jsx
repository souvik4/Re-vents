import React from "react";
import { useDispatch } from "react-redux";
import { closeModal } from "./modalReducer";
import { Modal } from "react-bootstrap";

export default function ModalWrapper({children, size, header}){
    const dispatch = useDispatch();
    return (
        <Modal show={true} onClose={() => dispatch(closeModal())} size={size} >
            {header && <Modal.Header>{header}</Modal.Header>}
            <Modal.Body>
                <>{children}</>  
            </Modal.Body>
        </Modal>
    )
}
