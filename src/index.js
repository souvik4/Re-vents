import React from 'react';
import ReactDOM from 'react-dom/client';
// import 'semantic-ui-css/semantic.min.css'
import 'react-toastify/dist/ReactToastify.css';
import 'react-calendar/dist/Calendar.css';
import './app/layout/styles.css';
import App from './app/layout/App';
import reportWebVitals from './reportWebVitals';
import { BrowserRouter } from 'react-router-dom';
import {Provider} from 'react-redux'
import { configureStore } from './app/store/configureStore';
import ScrollToTop from './app/layout/ScrollToTop';
import { loadEvent } from './features/events/eventActions';


const store = configureStore()
store.dispatch(loadEvent())

// const rootEl = document.getElementById('root')

// function render(){
//   ReactDOM.render(
//     <Provider store={store}>
//       <BrowserRouter>
//         <App />
//       </BrowserRouter>
//      </Provider>,
//   rootEl)
// }

const root = ReactDOM.createRoot(document.getElementById('root'));

const renderElement = () => {
  root.render(
    <Provider store={store}>
  <BrowserRouter>
    <ScrollToTop />
    <App />
  </BrowserRouter>
  </Provider>
  );
}

if(module.hot){
  module.hot.accept('./app/layout/App', function(){
    setTimeout(renderElement);
  })
}
renderElement();
// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();

